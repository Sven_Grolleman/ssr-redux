// startup point client side app
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import 'babel-polyfill';
import {renderRoutes} from 'react-router-config';
import reducers from './reducers';

import Routes from './Routes';
import Axios from 'axios';


const axiosInstance = Axios.create({
  baseURL: '/api'
});

const store = createStore(reducers, window.INITIAL_STATE, applyMiddleware(thunk.withExtraArgument(axiosInstance)));
delete window.INITIAL_STATE;

ReactDOM.hydrate(
  <Provider store={store}>
    <BrowserRouter >
      <div>{renderRoutes(Routes)}</div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('#root')
);
