import {combineReducers} from 'redux';
import usersReducer from './userReducer';
import authReducer from './AuthReducer';
import adminsReducers from './adminsReducers';

export default combineReducers({
  users: usersReducer,
  auth: authReducer,
  admins: adminsReducers
});
