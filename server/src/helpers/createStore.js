import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import reducers from '../client/reducers';
import Axios from 'axios';

export default (req) => {
  const axiosInstance = Axios.create({
    baseURL: 'http://react-ssr-api.herokuapp.com',
    headers: {cookie: req.get('cookie') || ''}
  })

  const store = createStore(reducers, {}, applyMiddleware(thunk.withExtraArgument(axiosInstance)));

  return store;
}


