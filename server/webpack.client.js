const path = require('path');
const merge = require('webpack-merge');
const base = require('./webpack.base');

const config = {
  // root file of server app
  entry: './src/client/client.js',
  // where to put the output file
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public'),
  }
};

module.exports = merge(base, config);