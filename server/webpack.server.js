const path = require('path');
const merge = require('webpack-merge');
const base = require('./webpack.base.js');
const webpackNodeExternals = require('webpack-node-externals');

const config = {
  // inform the bundling process for nodeJS
  target: 'node',
  // root file of server app
  entry: './src/index.js',
  // where to put the output file
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build'),
  },
  externals:[webpackNodeExternals()]
};

module.exports = merge(base, config);